[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/functions.zsh" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/functions.zsh"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/aliasrc.zsh" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/aliasrc.zsh"

# loading zsh modules
zmodload zsh/complist

# autoload stuff for git prompt and auto/tab completion
autoload -Uz vcs_info compinit	

# zsh modules
zstyle ':vcs_info:git:*' formats '%b ' # git stuff in prompt
zstyle ':completion:*' menu select # zsh comopletion 

# opts
unsetopt BEEP
setopt autocd extendedglob nomatch menucomplete interactive_comments PROMPT_SUBST HIST_IGNORE_SPACE SHARE_HISTORY  HIST_IGNORE_ALL_DUPS

# history 
HISTSIZE=10000000
SAVEHIST=10000000
if (( ! EUID )); then
	HISTFILE=~/.history_root
else
	HISTFILE=~/.history
fi

# vi mode
#bindkey -v
#export KEYTIMEOUT=1
# fix backspace bug when switching modes in vim mode
#bindkey "^?" backward-delete-char

# set up the prompt (with git branch name)
PROMPT='[%F{magenta}%n%F{yellow}@%F{cyan}%M %F{white}%~%B%F{red}${vcs_info_msg_0_}%f%b]%F{green}$ '

# basic auto/tab complete:
compinit
_comp_options+=(globdots) # include hidden files.

# stuff that's good
stty stop undef # Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')


source /home/nick/rbin/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh


