# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	bc="bc -ql" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best" \
	ffmpeg="ffmpeg -hide_banner" \
	cls='clear' \
	ls='exa'

alias suckcl="make clean && rm -rf config.h && git reset --hard origin/master"
alias suckin="make && sudo make clean install"
